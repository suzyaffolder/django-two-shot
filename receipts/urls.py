from django.contrib import admin
from django.urls import path
from receipts.views import receipt_list, create_receipt



urlpatterns = [
    path("", receipt_list, name="home"),
    path("", create_receipt, "create_receipt"),
]
